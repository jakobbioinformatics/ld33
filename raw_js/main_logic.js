"use strict";

var stage = new PIXI.Stage(0x66FF99);
var renderer = undefined;
var game_objects = [];
var overlay_container = undefined;

var FOREST_ENLARGED = PIXI.Texture.fromImage("art/Forest_enlarged.png");
var CREATURE_SHEET = PIXI.Texture.fromImage("art/CreatureSpriteSheet.png");
var ENEMY_SHEET = PIXI.Texture.fromImage("art/EnemySheet.png");
var FIRE_SHEET = PIXI.Texture.fromImage("art/Fire.png");
var TILE_SHEET = PIXI.Texture.fromImage("art/Tileset.png");
var SPRITE_SHEET = PIXI.Texture.fromImage("img/tileset16.png");
var PORTAL_SHEET = PIXI.Texture.fromImage("art/portal.png");

var FADE_SONG = new Howl({ urls: ['audio/fade_v2.mp3'], loop: true, volume: 0.2 });

var HURT_SOUND = new Howl({ urls: ['audio/hurt1.wav'] });
var COMPLETE_SOUND = new Howl({ urls: ['audio/Complete1.wav'] });
var JUMP_SOUND = new Howl({ urls: ['audio/jump4.wav'] });

var debug_view_on = false;

var LEVEL_WIDTH = 800;
var LEVEL_HEIGHT = 480;
var TILE_SIZE = 16;
var XTILES = LEVEL_WIDTH / TILE_SIZE;
var YTILES = LEVEL_HEIGHT / TILE_SIZE;
var TOTAL_TILES = XTILES * YTILES;
var stage_manager = new StageManagerInstance_temp();

function init() {

    renderer = PIXI.autoDetectRenderer(LEVEL_WIDTH, LEVEL_HEIGHT, { view: document.getElementById("game-canvas") });
    renderer.backgroundColor = 0x111111;
    requestAnimationFrame(update);
}

function update() {

    var game_time = new Date().getTime() / 1000;
    renderer.render(stage);
    requestAnimationFrame(update);
    stage_manager.update(game_time);
}

//# sourceMappingURL=main_logic.js.map