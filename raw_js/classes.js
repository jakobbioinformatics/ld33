// ------ Classes ------ //

'use strict';

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var GameObject = (function () {
    function GameObject(texture_sheet, static_rect, xpos, ypos) {
        _classCallCheck(this, GameObject);

        this.source_rect = static_rect;

        var base_text = new PIXI.Texture(texture_sheet.baseTexture, new PIXI.Rectangle(static_rect.x, static_rect.y, static_rect.width, static_rect.height));
        this.sprite = new PIXI.Sprite(base_text);
        this.animations = {};
        this.current_anim_name = 'static';
        this.animations[this.current_anim_name] = new Animation(texture_sheet, static_rect);
        this.current_anim = this.animations[this.current_anim_name];

        this.x = xpos;
        this.y = ypos;

        this.alpha = 1;
        this.movement = undefined;
        this.is_lightsource = false;

        this.logging = false;
    }

    _createClass(GameObject, [{
        key: 'get_sprite',
        value: function get_sprite() {
            return this.sprite;
        }
    }, {
        key: 'change_animation',
        value: function change_animation(name) {

            if (!name in this.animations) {
                throw new Error('Unknown animation: ' + name);
            }
            if (name === this.current_anim_name) {
                return;
            }
            this.current_anim_name = name;
            this.current_anim = this.animations[this.current_anim_name];
        }
    }, {
        key: 'update',
        value: function update(game_time, obstacles, game_objects) {

            if (this.movement !== undefined) {
                this.movement.update(game_time, obstacles);
            }

            if (!this.static_only) {
                this.current_anim.update(game_time);
                if (this.current_anim.has_new_frame()) {
                    this.sprite.texture = this.current_anim.get_current_frame();
                }
            }
        }
    }, {
        key: 'get_closest_object',
        value: function get_closest_object(objects) {

            var closest_obj = null;
            var closest_dist = 1000;
            for (var i = 0; i < objects.length; i++) {
                var obj = objects[i];
                var distance = this.get_dist_to_object(obj);
                if (distance < closest_dist) {
                    closest_dist = distance;
                    closest_obj = obj;
                }
            }
            return closest_obj;
        }
    }, {
        key: 'get_dist_to_object',
        value: function get_dist_to_object(other_obj) {
            return Math.sqrt(Math.pow(this.x - other_obj.x, 2) + Math.pow(this.y - other_obj.y, 2));
        }
    }, {
        key: 'x',
        get: function get() {
            return this.sprite.position.x;
        },
        set: function set(value) {
            this.sprite.position.x = value;
        }
    }, {
        key: 'y',
        get: function get() {
            return this.sprite.position.y;
        },
        set: function set(value) {
            this.sprite.position.y = value;
        }
    }, {
        key: 'width',
        get: function get() {
            return this.source_rect.width;
        }
    }, {
        key: 'height',
        get: function get() {
            return this.source_rect.height;
        }
    }, {
        key: 'alpha',
        get: function get() {
            return this.sprite.alpha;
        },
        set: function set(value) {
            this.sprite.alpha = value;
        }
    }]);

    return GameObject;
})();

var PlatformGameObject = (function (_GameObject) {
    _inherits(PlatformGameObject, _GameObject);

    function PlatformGameObject(spritesheet, rect, x, y) {
        _classCallCheck(this, PlatformGameObject);

        _get(Object.getPrototypeOf(PlatformGameObject.prototype), 'constructor', this).call(this, spritesheet, rect, x, y);
    }

    _createClass(PlatformGameObject, [{
        key: 'yspeed',
        get: function get() {
            return this.movement.yspeed;
        },
        set: function set(value) {
            this.movement.yspeed = value;
        }
    }, {
        key: 'is_jumping',
        get: function get() {
            return this.movement.is_jumping;
        },
        set: function set(value) {
            this.movement.is_jumping = value;
        }
    }, {
        key: 'grounded',
        get: function get() {
            return this.movement.grounded;
        },
        set: function set(value) {
            this.movement.grounded = value;
        }
    }]);

    return PlatformGameObject;
})(GameObject);

var Player = (function (_PlatformGameObject) {
    _inherits(Player, _PlatformGameObject);

    function Player(x, y) {
        _classCallCheck(this, Player);

        _get(Object.getPrototypeOf(Player.prototype), 'constructor', this).call(this, CREATURE_SHEET, new PIXI.Rectangle(0, 0, 16, 32), x, y);
        this.movement = new PlayerPlatformMovement(this);

        this.animations['static_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(17, 0, 16, 32));

        this.animations['walk_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 32, 16, 32), 4, 0.5);
        this.animations['walk_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(64, 32, 16, 32), 4, 0.5);

        this.animations['jump_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 64, 16, 30));
        this.animations['jump_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(16, 64, 16, 30));

        this.animations['hurt_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 94, 16, 32));
        this.animations['hurt_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(16, 94, 16, 32));

        this.MAX_HEALTH = 80;
        this.current_health = this.MAX_HEALTH;

        this.light_sources = [];
        this.level_win = false;

        this.taking_damage = false;

        this.LIGHT_DAMAGE = 1;
        this.REGENERATION = 0.16;

        this.next_hurt_sound = 0;
    }

    _createClass(Player, [{
        key: 'update',
        value: function update(game_time, obstacles, game_objects) {
            _get(Object.getPrototypeOf(Player.prototype), 'update', this).call(this, game_time, obstacles, game_objects);

            for (var i = 0; i < game_objects.length; i++) {
                var obj = game_objects[i];

                if (obj === this) {
                    continue;
                }

                if (intersectObjects(this, obj)) {
                    if (obj instanceof GoalSquare) {
                        this.level_win = true;
                        COMPLETE_SOUND.play();
                        //stage_manager.change_stage('win');
                    }
                }
            }

            this._update_light_damage();

            if (this.current_health < this.MAX_HEALTH && !this.taking_damage) {
                this.current_health += this.REGENERATION;
            }

            if (this.taking_damage && game_time > this.next_hurt_sound) {
                HURT_SOUND.play();
                this.next_hurt_sound = game_time + 0.5;
            } else {
                HURT_SOUND.stop();
            }

            this._update_anim_choice();
            this.alpha = this.current_health / this.MAX_HEALTH;
        }
    }, {
        key: 'assign_light_sources',
        value: function assign_light_sources(light_sources) {
            this.light_sources = light_sources;
        }
    }, {
        key: '_update_light_damage',
        value: function _update_light_damage() {
            this.taking_damage = false;
            if (this.light_sources.length > 0) {
                var closest = this.get_closest_object(this.light_sources);
                var closest_dist = this.get_dist_to_object(closest);
                if (closest_dist < LIGHT_RADIUS) {
                    this.current_health -= this.LIGHT_DAMAGE;
                    this.taking_damage = true;
                }
            }
        }
    }, {
        key: '_update_anim_choice',
        value: function _update_anim_choice() {
            if (this.taking_damage) {
                if (this.movement.xspeed > 0) {
                    this.change_animation('hurt_r');
                } else {
                    this.change_animation('hurt_l');
                }
            } else if (this.movement.is_jumping) {
                if (this.movement.xspeed > 0) {
                    this.change_animation('jump_r');
                } else {
                    this.change_animation('jump_l');
                }
            } else if (Math.abs(this.movement.xspeed) > 0.1) {
                if (this.movement.xspeed > 0) {
                    this.change_animation('walk_r');
                } else {
                    this.change_animation('walk_l');
                }
            } else {
                if (this.movement.xspeed >= 0) {
                    this.change_animation('static');
                } else {
                    this.change_animation('static_l');
                }
            }
        }
    }, {
        key: 'is_dead',
        get: function get() {
            return this.current_health <= 0;
        }
    }]);

    return Player;
})(PlatformGameObject);

var EnemyCharacter = (function (_PlatformGameObject2) {
    _inherits(EnemyCharacter, _PlatformGameObject2);

    function EnemyCharacter(x, y) {
        _classCallCheck(this, EnemyCharacter);

        var yoff = -7;
        _get(Object.getPrototypeOf(EnemyCharacter.prototype), 'constructor', this).call(this, ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), x, y + yoff);
        this.animations['static_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), 2, 1);
        this.animations['static_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(32, 0, 16, 23), 2, 1);

        this.animations['walk_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 23, 16, 23), 4, 1);
        this.animations['walk_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(64, 23, 16, 23), 4, 1);

        this.movement = new EnemyPlatformMovement(this);

        this.change_animation('static_l');
        this.is_lightsource = true;

        this.SIGHT_RANGE = 150;
        this.STOP_RANGE = 20;
        this.player_dir = Direction.NONE;
    }

    _createClass(EnemyCharacter, [{
        key: 'update',
        value: function update(game_time, obstacles, game_objects) {
            _get(Object.getPrototypeOf(EnemyCharacter.prototype), 'update', this).call(this, game_time, obstacles, game_objects);

            var player_ref = undefined;
            for (var i = 0; i < game_objects.length; i++) {
                var obj = game_objects[i];
                if (obj instanceof Player) {
                    player_ref = obj;
                    break;
                }
            }

            if (player_ref) {
                this.player_dir = this._get_player_dir(player_ref);
            }

            this._update_anim_choice();
        }
    }, {
        key: '_update_anim_choice',
        value: function _update_anim_choice() {
            if (Math.abs(this.movement.xspeed) > 0.1) {
                if (this.movement.xspeed > 0) {
                    this.change_animation('walk_r');
                } else {
                    this.change_animation('walk_l');
                }
            } else {
                if (this.movement.xspeed >= 0) {
                    this.change_animation('static_r');
                } else {
                    this.change_animation('static_l');
                }
            }
        }
    }, {
        key: '_get_player_dir',
        value: function _get_player_dir(player_ref) {

            var dist = this.get_dist_to_object(player_ref);
            if (dist < this.SIGHT_RANGE && dist > this.STOP_RANGE) {
                if (this.x < player_ref.x) {
                    return Direction.RIGHT;
                } else {
                    return Direction.LEFT;
                }
            } else {
                return Direction.NONE;
            }
        }
    }]);

    return EnemyCharacter;
})(PlatformGameObject);

var StaticSquare = (function (_GameObject2) {
    _inherits(StaticSquare, _GameObject2);

    function StaticSquare(x, y, tile_nbr) {
        _classCallCheck(this, StaticSquare);

        tile_nbr -= 1;

        var xpos = tile_nbr % 3 * TILE_SIZE;
        var ypos = Math.floor(tile_nbr / 3) * TILE_SIZE;

        var rect = new PIXI.Rectangle(xpos, ypos, TILE_SIZE, TILE_SIZE);
        _get(Object.getPrototypeOf(StaticSquare.prototype), 'constructor', this).call(this, TILE_SHEET, rect, x, y);
    }

    return StaticSquare;
})(GameObject);

var GoalSquare = (function (_GameObject3) {
    _inherits(GoalSquare, _GameObject3);

    function GoalSquare(x, y) {
        _classCallCheck(this, GoalSquare);

        _get(Object.getPrototypeOf(GoalSquare.prototype), 'constructor', this).call(this, PORTAL_SHEET, new PIXI.Rectangle(0, 0, 16, 32), x, y - 16);
    }

    return GoalSquare;
})(GameObject);

var LightEmitterSquare = (function (_GameObject4) {
    _inherits(LightEmitterSquare, _GameObject4);

    function LightEmitterSquare(x, y) {
        _classCallCheck(this, LightEmitterSquare);

        _get(Object.getPrototypeOf(LightEmitterSquare.prototype), 'constructor', this).call(this, FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), x, y);
        this.is_lightsource = true;
        this.animations['fire'] = new Animation(FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), 3, 1);
        this.current_anim = this.animations['fire'];
    }

    return LightEmitterSquare;
})(GameObject);

//# sourceMappingURL=classes.js.map