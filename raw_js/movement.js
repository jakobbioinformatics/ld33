'use strict';

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Direction = {
    'LEFT': 1,
    'UP': 2,
    'RIGHT': 3,
    'DOWN': 4,
    'NONE': 0
};

var EdgeBehaviour = {
    'NONE': 1,
    'STOP': 2,
    'STOP_PLATFORM': 3,
    'BOUNCE': 4
};

var Movement = (function () {
    function Movement(game_object) {
        _classCallCheck(this, Movement);

        this.my_obj = game_object;
        this.xspeed = 0;
        this.yspeed = 0;
        this.edge_behaviour = EdgeBehaviour.NONE;
    }

    _createClass(Movement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            this.my_obj.x += this.xspeed;

            if (!this.grounded) {
                this.my_obj.y += this.yspeed;
            }

            if (this.edge_behaviour !== EdgeBehaviour.NONE) {
                this._handle_edge_behaviour();
            }
        }
    }, {
        key: '_handle_edge_behaviour',
        value: function _handle_edge_behaviour() {
            var edge_collisions = this.get_edge_collisions();
            if (edge_collisions.length > 0) {
                if (this.edge_behaviour == EdgeBehaviour.BOUNCE) {
                    this._bounce_at_edges(edge_collisions);
                } else if (this.edge_behaviour == EdgeBehaviour.STOP || this.edge_behaviour == EdgeBehaviour.STOP_PLATFORM) {
                    this._stop_at_edges(this.edge_behaviour, edge_collisions);
                } else {
                    throw new Error("Unexpected edge behaviour: " + this.edge_behaviour);
                }
            }
        }
    }, {
        key: '_bounce_at_edges',
        value: function _bounce_at_edges(edges) {
            for (var i = 0; i < edges.length; i++) {
                switch (edges[i]) {
                    case Direction.LEFT:
                        {
                            this.xspeed = Math.abs(this.xspeed);
                            break;
                        }
                    case Direction.UP:
                        {
                            this.yspeed = Math.abs(this.yspeed);
                            break;
                        }
                    case Direction.RIGHT:
                        {
                            this.xspeed = -Math.abs(this.xspeed);
                            break;
                        }
                    case Direction.DOWN:
                        {
                            this.yspeed = -Math.abs(this.yspeed);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }, {
        key: '_stop_at_edges',
        value: function _stop_at_edges(behaviour, edges) {
            for (var i = 0; i < edges.length; i++) {
                switch (edges[i]) {
                    case Direction.LEFT:
                        {
                            this.my_obj.x = 0;
                            break;
                        }
                    case Direction.UP:
                        {
                            this.my_obj.y = 0;
                            if (behaviour === EdgeBehaviour.STOP_PLATFORM) {
                                this.yspeed = 0;
                            }
                            break;
                        }
                    case Direction.RIGHT:
                        {
                            this.my_obj.x = LEVEL_WIDTH - this.my_obj.width;
                            break;
                        }
                    case Direction.DOWN:
                        {
                            this.my_obj.y = LEVEL_HEIGHT - this.my_obj.height;
                            if (behaviour === EdgeBehaviour.STOP_PLATFORM) {
                                this.is_jumping = false;
                                this.grounded = true;
                                this.yspeed = 0;
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }, {
        key: 'get_edge_collisions',
        value: function get_edge_collisions() {

            var edge_collisions = [];

            if (this.my_obj.x < 0) {
                edge_collisions.push(Direction.LEFT);
            } else if (this.my_obj.x + this.my_obj.width > LEVEL_WIDTH) {
                edge_collisions.push(Direction.RIGHT);
            }

            //if (this.my_obj.y < 0) {
            //    edge_collisions.push(Direction.UP);
            //}
            if (this.my_obj.y + this.my_obj.height > LEVEL_HEIGHT) {
                edge_collisions.push(Direction.DOWN);
            }

            return edge_collisions;
        }
    }]);

    return Movement;
})();

var RandomBounceMovement = (function (_Movement) {
    _inherits(RandomBounceMovement, _Movement);

    function RandomBounceMovement(game_object) {
        _classCallCheck(this, RandomBounceMovement);

        _get(Object.getPrototypeOf(RandomBounceMovement.prototype), 'constructor', this).call(this, game_object);

        this.xspeed = 2 * Math.random() * this.speed - this.speed;
        this.yspeed = 2 * Math.random() * this.speed - this.speed;

        this.edge_behaviour = EdgeBehaviour.BOUNCE;
    }

    _createClass(RandomBounceMovement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            _get(Object.getPrototypeOf(RandomBounceMovement.prototype), 'update', this).call(this, game_time, obstacles);
        }
    }]);

    return RandomBounceMovement;
})(Movement);

var PlatformMovement = (function (_Movement2) {
    _inherits(PlatformMovement, _Movement2);

    function PlatformMovement(game_objects) {
        _classCallCheck(this, PlatformMovement);

        _get(Object.getPrototypeOf(PlatformMovement.prototype), 'constructor', this).call(this, game_objects);

        this.GRAVITY = 0.3;

        this.FRICTION = 0.8;
        this.MAX_SPEED = 1.5;
        this.JUMPSTRENGTH = 0;

        this.edge_behaviour = EdgeBehaviour.STOP_PLATFORM;
        this.is_jumping = false;
        this.grounded = false;
    }

    _createClass(PlatformMovement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            _get(Object.getPrototypeOf(PlatformMovement.prototype), 'update', this).call(this, game_time, obstacles);

            this.xspeed *= this.FRICTION;
            this.yspeed += this.GRAVITY;

            this._update_obstacle_collision(obstacles);
        }
    }, {
        key: '_update_obstacle_collision',
        value: function _update_obstacle_collision(obstacles) {

            this.grounded = false;
            for (var i = 0; i < obstacles.length; i++) {

                var obj = obstacles[i];
                var dir = correctingColCheck(this.my_obj, obj);

                if (dir === Direction.LEFT || dir === Direction.RIGHT) {
                    this.is_jumping = false;
                } else if (dir === Direction.DOWN) {
                    this.is_jumping = false;
                    this.grounded = true;
                    this.yspeed = 0;
                } else if (dir === Direction.UP) {
                    this.yspeed = 0;
                }
            }
        }
    }]);

    return PlatformMovement;
})(Movement);

var PlayerPlatformMovement = (function (_PlatformMovement) {
    _inherits(PlayerPlatformMovement, _PlatformMovement);

    function PlayerPlatformMovement(game_object) {
        _classCallCheck(this, PlayerPlatformMovement);

        _get(Object.getPrototypeOf(PlayerPlatformMovement.prototype), 'constructor', this).call(this, game_object);

        this.keyboard = new Keyboard();
        this.FRICTION = 0.8;
        this.MAX_SPEED = 1.8;
        this.JUMPSTRENGTH = 7.5;
    }

    _createClass(PlayerPlatformMovement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            _get(Object.getPrototypeOf(PlayerPlatformMovement.prototype), 'update', this).call(this, game_time, obstacles);
            this._update_keyboard_controls();
        }
    }, {
        key: '_update_keyboard_controls',
        value: function _update_keyboard_controls() {

            if (this.keyboard.isKeyPressed('left')) {
                this.xspeed = -this.MAX_SPEED;
            } else if (this.keyboard.isKeyPressed('right')) {
                this.xspeed = this.MAX_SPEED;
            }

            if (this.keyboard.isKeyPressed('up') && !this.is_jumping && this.grounded) {
                this._jump();
            }
        }
    }, {
        key: '_jump',
        value: function _jump() {
            this.is_jumping = true;
            this.grounded = false;
            this.yspeed = -this.JUMPSTRENGTH;

            JUMP_SOUND.play();
        }
    }]);

    return PlayerPlatformMovement;
})(PlatformMovement);

var EnemyPlatformMovement = (function (_PlatformMovement2) {
    _inherits(EnemyPlatformMovement, _PlatformMovement2);

    function EnemyPlatformMovement(game_object) {
        _classCallCheck(this, EnemyPlatformMovement);

        _get(Object.getPrototypeOf(EnemyPlatformMovement.prototype), 'constructor', this).call(this, game_object);

        this.keyboard = new Keyboard();
        this.FRICTION = 0.8;
        this.MAX_SPEED = 0.5;
        this.JUMPSTRENGTH = 7;
    }

    _createClass(EnemyPlatformMovement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            _get(Object.getPrototypeOf(EnemyPlatformMovement.prototype), 'update', this).call(this, game_time, obstacles);

            if (this.my_obj.player_dir === Direction.LEFT) {
                this.xspeed = -this.MAX_SPEED;
            } else if (this.my_obj.player_dir === Direction.RIGHT) {
                this.xspeed = this.MAX_SPEED;
            }
        }
    }]);

    return EnemyPlatformMovement;
})(PlatformMovement);

var PlayerTopDownMovement = (function (_Movement3) {
    _inherits(PlayerTopDownMovement, _Movement3);

    function PlayerTopDownMovement(game_object) {
        _classCallCheck(this, PlayerTopDownMovement);

        _get(Object.getPrototypeOf(PlayerTopDownMovement.prototype), 'constructor', this).call(this, game_object);

        this.keyboard = new Keyboard();
    }

    _createClass(PlayerTopDownMovement, [{
        key: 'update',
        value: function update(game_time, obstacles) {
            _get(Object.getPrototypeOf(PlayerTopDownMovement.prototype), 'update', this).call(this, game_time, obstacles);
            this._update_keyboard_controls();
        }
    }, {
        key: '_update_keyboard_controls',
        value: function _update_keyboard_controls() {

            var hasMoved = false;

            if (this.keyboard.isKeyPressed('left')) {
                hasMoved = true;
            } else if (this.keyboard.isKeyPressed('right')) {
                hasMoved = true;
            }

            if (this.keyboard.isKeyPressed('up')) {
                hasMoved = true;
            } else if (this.keyboard.isKeyPressed('down')) {
                hasMoved = true;
            }

            if (hasMoved) {
                this.speed = 5;
            } else {
                this.speed = 0;
            }
        }
    }]);

    return PlayerTopDownMovement;
})(Movement);

//# sourceMappingURL=movement.js.map