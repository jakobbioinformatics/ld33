'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Keyboard = (function () {

    // Allows the user to specify keycodes that are evaluated as pressed or not pressed
    // The user is able to get the status of a particular key

    function Keyboard() {
        _classCallCheck(this, Keyboard);

        this.keys = {};
        this._setup_default_keys();
    }

    _createClass(Keyboard, [{
        key: '_setup_default_keys',
        value: function _setup_default_keys() {
            this.keys['left'] = new Key(37);
            this.keys['up'] = new Key(38);
            this.keys['right'] = new Key(39);
            this.keys['down'] = new Key(40);
            this.keys['space'] = new Key(32);
        }
    }, {
        key: 'addKey',
        value: function addKey(keycode) {
            this.keys[keycode] = new Key(keycode);
        }
    }, {
        key: 'isKeyPressed',
        value: function isKeyPressed(keycode) {
            return this.keys[keycode].isDown;
        }
    }]);

    return Keyboard;
})();

var Key =

// Represents a single key
// Registers the given keycode, and tracks if it is pressed or not

function Key(keycode) {
    _classCallCheck(this, Key);

    var that = this;
    this.code = keycode;
    this.isDown = false;
    this.isUp = true;

    this.press = undefined;
    this.release = undefined;

    this.downHandler = function (event) {

        if (event.keyCode === that.code) {

            if (that.isUp) {
                that.isDown = true;
                that.isUp = false;
            }
        }
        event.preventDefault();
    };

    this.upHandler = function (event) {
        if (event.keyCode === that.code) {
            that.isDown = false;
            that.isUp = true;
        }
        event.preventDefault();
    };

    //Attach event listeners
    window.addEventListener("keydown", this.downHandler.bind(this.key), false);
    window.addEventListener("keyup", this.upHandler.bind(this.key), false);
};

//# sourceMappingURL=keyboard.js.map