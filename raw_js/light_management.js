"use strict";

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LIGHT_RADIUS = 100;

var LightManager = (function () {
    function LightManager() {
        _classCallCheck(this, LightManager);

        this.BASE_OPACITY = 0.6;
        this.dark_matrix = [];
        this.dark_list = [];
        this.light_sources = undefined;
    }

    _createClass(LightManager, [{
        key: "assign_light_sources",
        value: function assign_light_sources(game_objects) {
            this.light_sources = [];
            for (var i = 0; i < game_objects.length; i++) {
                var obj = game_objects[i];
                if (obj.is_lightsource) {
                    this.light_sources.push(obj);
                }
            }
        }
    }, {
        key: "generate_darkness",
        value: function generate_darkness() {
            for (var x = 0; x < XTILES; x++) {
                var col = [];
                for (var y = 0; y < XTILES; y++) {
                    var sq = new DarkSquare(x * TILE_SIZE, y * TILE_SIZE, this.BASE_OPACITY);
                    col.push(sq);
                    this.dark_list.push(sq);
                }
                this.dark_matrix.push(col);
            }
        }
    }, {
        key: "update",
        value: function update() {
            for (var x = 0; x < XTILES; x++) {
                for (var y = 0; y < YTILES; y++) {
                    this.dark_matrix[x][y].update(this.light_sources);
                }
            }
        }
    }, {
        key: "get_light_sources",
        value: function get_light_sources() {
            return this.light_sources;
        }
    }]);

    return LightManager;
})();

var DarkSquare = (function (_GameObject) {
    _inherits(DarkSquare, _GameObject);

    function DarkSquare(x, y, base_alpha) {
        _classCallCheck(this, DarkSquare);

        _get(Object.getPrototypeOf(DarkSquare.prototype), "constructor", this).call(this, SPRITE_SHEET, new PIXI.Rectangle(32, 16, TILE_SIZE, TILE_SIZE), x, y);

        this.base_alpha = base_alpha;
        this.alpha = this.base_alpha;
        //overlay_container.addChild(this.get_sprite());
        stage.addChild(this.get_sprite());
    }

    _createClass(DarkSquare, [{
        key: "update",
        value: function update(light_sources) {

            if (light_sources.length > 0) {
                var closest_source = this.get_closest_object(light_sources);
                var closest_dist = this.get_dist_to_object(closest_source);

                if (closest_dist < LIGHT_RADIUS) {
                    this.alpha = this.base_alpha * (closest_dist / LIGHT_RADIUS);
                }
            }
        }

        //_get_closest_distance(light_sources) {
        //    let closest_dist = 1000;
        //    for (let i = 0;  i < light_sources.length; i++) {
        //        let source = light_sources[i];
        //        let distance = this._get_distance(source.x, source.y);
        //        if (distance < closest_dist) {
        //            closest_dist = distance;
        //        }
        //    }
        //    return closest_dist;
        //}

        //_get_distance(other_x, other_y) {
        //
        //    //alert(`${this.x}, ${this.y}, ${other_x}, ${other_y}`);
        //    return Math.sqrt(Math.pow(this.x - other_x, 2) + Math.pow(this.y - other_y, 2));
        //}
    }]);

    return DarkSquare;
})(GameObject);

//# sourceMappingURL=light_management.js.map