// ------ Classes ------ //

class GameObject {

    constructor(texture_sheet, static_rect, xpos, ypos) {

        this.source_rect = static_rect;

        let base_text = new PIXI.Texture(texture_sheet.baseTexture, new PIXI.Rectangle(static_rect.x, static_rect.y, static_rect.width, static_rect.height));
        this.sprite = new PIXI.Sprite(base_text);
        this.animations = {};
        this.current_anim_name = 'static';
        this.animations[this.current_anim_name] = new Animation(texture_sheet, static_rect);
        this.current_anim = this.animations[this.current_anim_name];

        this.x = xpos;
        this.y = ypos;

        this.alpha = 1;
        this.movement = undefined;
        this.is_lightsource = false;

        this.logging = false;
    }

    get_sprite() { return this.sprite; }
    change_animation(name) {

        if (!name in this.animations) {
            throw new Error(`Unknown animation: ${name}`);
        }
        if (name === this.current_anim_name) {
            return;
        }
        this.current_anim_name = name;
        this.current_anim = this.animations[this.current_anim_name];
    }

    get x() { return this.sprite.position.x; };
    set x(value) { this.sprite.position.x = value; };
    get y() { return this.sprite.position.y; };
    set y(value) { this.sprite.position.y = value; };

    get width() { return this.source_rect.width; }
    get height() { return this.source_rect.height; }

    get alpha() { return this.sprite.alpha; }
    set alpha(value) { this.sprite.alpha = value; }

    update(game_time, obstacles, game_objects) {

        if (this.movement !== undefined) {
            this.movement.update(game_time, obstacles);
        }

        if (!this.static_only) {
            this.current_anim.update(game_time);
            if (this.current_anim.has_new_frame()) {
                this.sprite.texture = this.current_anim.get_current_frame();
            }
        }
    }

    get_closest_object(objects) {

        let closest_obj = null;
        let closest_dist = 1000;
        for (let i = 0;  i < objects.length; i++) {
            let obj = objects[i];
            let distance = this.get_dist_to_object(obj);
            if (distance < closest_dist) {
                closest_dist = distance;
                closest_obj = obj;
            }
        }
        return closest_obj;
    }

    get_dist_to_object(other_obj) {
        return Math.sqrt(Math.pow(this.x - other_obj.x, 2) + Math.pow(this.y - other_obj.y, 2));
    }
}

class PlatformGameObject extends GameObject {

    constructor(spritesheet, rect, x, y) {
        super(spritesheet, rect, x, y);
    }

    get yspeed() {
        return this.movement.yspeed;
    }

    set yspeed(value) {
        this.movement.yspeed = value;
    }

    get is_jumping() {
        return this.movement.is_jumping;
    }

    set is_jumping(value) {
        this.movement.is_jumping = value;
    }

    get grounded() {
        return this.movement.grounded;
    }

    set grounded(value) {
        this.movement.grounded = value;
    }
}

class Player extends PlatformGameObject {

    constructor(x, y) {
        super(CREATURE_SHEET, new PIXI.Rectangle(0, 0, 16, 32), x, y);
        this.movement = new PlayerPlatformMovement(this);

        this.animations['static_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(17, 0, 16, 32));

        this.animations['walk_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 32, 16, 32), 4, 0.5);
        this.animations['walk_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(64, 32, 16, 32), 4, 0.5);

        this.animations['jump_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 64, 16, 30));
        this.animations['jump_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(16, 64, 16, 30));

        this.animations['hurt_r'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(0, 94, 16, 32));
        this.animations['hurt_l'] = new Animation(CREATURE_SHEET, new PIXI.Rectangle(16, 94, 16, 32));

        this.MAX_HEALTH = 80;
        this.current_health = this.MAX_HEALTH;

        this.light_sources = [];
        this.level_win = false;

        this.taking_damage = false;

        this.LIGHT_DAMAGE = 1;
        this.REGENERATION = 0.16;

        this.next_hurt_sound = 0;
    }

    get is_dead() { return this.current_health <= 0; }

    update(game_time, obstacles, game_objects) {
        super.update(game_time, obstacles, game_objects);

        for (let i = 0; i < game_objects.length; i++) {
            let obj = game_objects[i];

            if (obj === this) {
                continue;
            }

            if (intersectObjects(this, obj)) {
                if (obj instanceof GoalSquare) {
                    this.level_win = true;
                    COMPLETE_SOUND.play();
                    //stage_manager.change_stage('win');
                }
            }
        }

        this._update_light_damage();

        if (this.current_health < this.MAX_HEALTH && !this.taking_damage) {
            this.current_health += this.REGENERATION;
        }

        if (this.taking_damage && game_time > this.next_hurt_sound) {
            HURT_SOUND.play();
            this.next_hurt_sound = game_time + 0.5;
        }
        else {
            HURT_SOUND.stop();
        }

        this._update_anim_choice();
        this.alpha = this.current_health / this.MAX_HEALTH;
    }

    assign_light_sources(light_sources) {
        this.light_sources = light_sources;
    }

    _update_light_damage() {
        this.taking_damage = false;
        if (this.light_sources.length > 0) {
            let closest = this.get_closest_object(this.light_sources);
            let closest_dist = this.get_dist_to_object(closest);
            if (closest_dist < LIGHT_RADIUS) {
                this.current_health -= this.LIGHT_DAMAGE;
                this.taking_damage = true;
            }
        }
    }

    _update_anim_choice() {
        if (this.taking_damage) {
            if (this.movement.xspeed > 0) {
                this.change_animation('hurt_r');
            }
            else {
                this.change_animation('hurt_l');
            }
        }
        else if (this.movement.is_jumping) {
            if (this.movement.xspeed > 0) {
                this.change_animation('jump_r');
            }
            else {
                this.change_animation('jump_l');
            }
        }
        else if (Math.abs(this.movement.xspeed) > 0.1) {
            if (this.movement.xspeed > 0) {
                this.change_animation('walk_r');
            }
            else {
                this.change_animation('walk_l');
            }
        }
        else {
            if (this.movement.xspeed >= 0) {
                this.change_animation('static');
            }
            else {
                this.change_animation('static_l');
            }
        }
    }
}

class EnemyCharacter extends PlatformGameObject {

    constructor(x, y) {
        let yoff = -7;
        super(ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), x, y+yoff);
        this.animations['static_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), 2, 1);
        this.animations['static_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(32, 0, 16, 23), 2, 1);

        this.animations['walk_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 23, 16, 23), 4, 1);
        this.animations['walk_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(64, 23, 16, 23), 4, 1);

        this.movement = new EnemyPlatformMovement(this);

        this.change_animation('static_l');
        this.is_lightsource = true;

        this.SIGHT_RANGE = 150;
        this.STOP_RANGE = 20;
        this.player_dir = Direction.NONE;
    }

    update(game_time, obstacles, game_objects) {
        super.update(game_time, obstacles, game_objects);

        let player_ref;
        for (let i = 0; i < game_objects.length; i++) {
            let obj = game_objects[i];
            if (obj instanceof Player) {
                player_ref = obj;
                break;
            }
        }

        if (player_ref) {
            this.player_dir = this._get_player_dir(player_ref);
        }

        this._update_anim_choice();
    }

    _update_anim_choice() {
        if (Math.abs(this.movement.xspeed) > 0.1) {
            if (this.movement.xspeed > 0) {
                this.change_animation('walk_r');
            }
            else {
                this.change_animation('walk_l');
            }
        }
        else {
            if (this.movement.xspeed >= 0) {
                this.change_animation('static_r');
            }
            else {
                this.change_animation('static_l');
            }
        }
    }

    _get_player_dir(player_ref) {

        let dist = this.get_dist_to_object(player_ref);
        if (dist < this.SIGHT_RANGE && dist > this.STOP_RANGE) {
            if (this.x < player_ref.x) {
                return Direction.RIGHT;
            }
            else {
                return Direction.LEFT;
            }
        }
        else {
            return Direction.NONE;
        }
    }
}

class StaticSquare extends GameObject {

    constructor(x, y, tile_nbr) {
        tile_nbr -= 1;

        let xpos = tile_nbr % 3 * TILE_SIZE;
        let ypos = Math.floor(tile_nbr / 3) * TILE_SIZE;

        let rect = new PIXI.Rectangle(xpos, ypos, TILE_SIZE, TILE_SIZE);
        super(TILE_SHEET, rect, x, y);
    }
}

class GoalSquare extends GameObject {

    constructor(x, y) {
        super(PORTAL_SHEET, new PIXI.Rectangle(0, 0, 16, 32), x, y-16);
    }
}

class LightEmitterSquare extends GameObject {

    constructor(x, y) {
        super(FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), x, y);
        this.is_lightsource = true;
        this.animations['fire'] = new Animation(FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), 3, 1);
        this.current_anim = this.animations['fire'];
    }
}