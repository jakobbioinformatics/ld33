"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Animation = (function () {
    function Animation(texture_sheet, rect) {
        var nbr_frames = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];
        var loop_time = arguments.length <= 3 || arguments[3] === undefined ? 0.1 : arguments[3];

        _classCallCheck(this, Animation);

        this.frames = [];
        for (var i = 0; i < nbr_frames; i++) {
            var source_rect = new PIXI.Rectangle(rect.x + i * rect.width, rect.y, rect.width, rect.height);
            this.frames.push(new PIXI.Texture(texture_sheet.baseTexture, source_rect));
        }

        this.current_frame = 0;
        this.delta_anim_time = loop_time / this.frames.length;
        this.next_anim_time = 0;

        this.new_frame_ready = false;
    }

    _createClass(Animation, [{
        key: "update",
        value: function update(game_time) {

            if (this.next_anim_time === 0) {
                this.next_anim_time = game_time + this.delta_anim_time;
            } else if (game_time >= this.next_anim_time) {
                this.current_frame++;
                if (this.current_frame >= this.frames.length) {
                    this.current_frame = 0;
                }

                this.next_anim_time = game_time + this.delta_anim_time;
                this.new_frame_ready = true;
            }
        }
    }, {
        key: "has_new_frame",
        value: function has_new_frame() {
            return this.new_frame_ready;
        }
    }, {
        key: "get_current_frame",
        value: function get_current_frame() {
            this.new_frame_ready = false;
            return this.frames[this.current_frame];
        }
    }]);

    return Animation;
})();

//# sourceMappingURL=animation.js.map