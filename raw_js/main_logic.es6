let stage = new PIXI.Stage(0x66FF99);
let renderer;
let game_objects = [];
let overlay_container;

let FOREST_ENLARGED = PIXI.Texture.fromImage("art/Forest_enlarged.png");
let CREATURE_SHEET = PIXI.Texture.fromImage("art/CreatureSpriteSheet.png");
let ENEMY_SHEET = PIXI.Texture.fromImage("art/EnemySheet.png");
let FIRE_SHEET = PIXI.Texture.fromImage("art/Fire.png");
let TILE_SHEET = PIXI.Texture.fromImage("art/Tileset.png");
let SPRITE_SHEET = PIXI.Texture.fromImage("img/tileset16.png");
let PORTAL_SHEET = PIXI.Texture.fromImage("art/portal.png");

let FADE_SONG = new Howl({urls: ['audio/fade_v2.mp3'], loop: true, volume: 0.2});

let HURT_SOUND = new Howl({urls: ['audio/hurt1.wav']});
let COMPLETE_SOUND = new Howl({urls: ['audio/Complete1.wav']});
let JUMP_SOUND = new Howl({urls: ['audio/jump4.wav']});

let debug_view_on = false;

const LEVEL_WIDTH = 800;
const LEVEL_HEIGHT = 480;
const TILE_SIZE = 16;
const XTILES = LEVEL_WIDTH / TILE_SIZE;
const YTILES = LEVEL_HEIGHT / TILE_SIZE;
const TOTAL_TILES = XTILES * YTILES;
let stage_manager = new StageManagerInstance_temp();

function init() {

    renderer = PIXI.autoDetectRenderer(LEVEL_WIDTH, LEVEL_HEIGHT, {view:document.getElementById("game-canvas")});
    renderer.backgroundColor = 0x111111;
    requestAnimationFrame(update);
}

function update() {

    let game_time = new Date().getTime() / 1000;
    renderer.render(stage);
    requestAnimationFrame(update);
    stage_manager.update(game_time);
}


