
let LIGHT_RADIUS = 100;

class LightManager {

    constructor() {

        this.BASE_OPACITY = 0.6;
        this.dark_matrix = [];
        this.dark_list = [];
        this.light_sources = undefined;
    }

    assign_light_sources(game_objects) {
        this.light_sources = [];
        for (let i = 0; i < game_objects.length; i++) {
            let obj = game_objects[i];
            if (obj.is_lightsource)  {
                this.light_sources.push(obj);
            }
        }
    }

    generate_darkness() {
        for (let x = 0; x < XTILES; x++) {
            let col = [];
            for (let y = 0; y < XTILES; y++) {
                let sq = new DarkSquare(x * TILE_SIZE, y * TILE_SIZE, this.BASE_OPACITY);
                col.push(sq);
                this.dark_list.push(sq)
            }
            this.dark_matrix.push(col);
        }
    }

    update() {
        for (let x = 0; x < XTILES; x++) {
            for (let y = 0; y < YTILES; y++) {
                this.dark_matrix[x][y].update(this.light_sources);
            }
        }
    }

    get_light_sources() {
        return this.light_sources;
    }
}

class DarkSquare extends GameObject {

    constructor(x, y, base_alpha) {
        super(SPRITE_SHEET, new PIXI.Rectangle(32, 16, TILE_SIZE, TILE_SIZE), x, y);

        this.base_alpha = base_alpha;
        this.alpha = this.base_alpha;
        //overlay_container.addChild(this.get_sprite());
        stage.addChild(this.get_sprite());
    }

    update(light_sources) {

        if (light_sources.length > 0) {
            let closest_source = this.get_closest_object(light_sources);
            let closest_dist = this.get_dist_to_object(closest_source);

            if (closest_dist < LIGHT_RADIUS) {
                this.alpha = this.base_alpha * (closest_dist / LIGHT_RADIUS);
            }
        }
    }

    //_get_closest_distance(light_sources) {
    //    let closest_dist = 1000;
    //    for (let i = 0;  i < light_sources.length; i++) {
    //        let source = light_sources[i];
    //        let distance = this._get_distance(source.x, source.y);
    //        if (distance < closest_dist) {
    //            closest_dist = distance;
    //        }
    //    }
    //    return closest_dist;
    //}

    //_get_distance(other_x, other_y) {
    //
    //    //alert(`${this.x}, ${this.y}, ${other_x}, ${other_y}`);
    //    return Math.sqrt(Math.pow(this.x - other_x, 2) + Math.pow(this.y - other_y, 2));
    //}
}
