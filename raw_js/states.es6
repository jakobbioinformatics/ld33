class StageManagerInstance_temp {

    constructor() {

        this.stages = {};
        this.stages['menu'] = new MenuState();
        this.stages['level'] = new LevelState();
        this.stages['win'] = new WinState();

        this.current_stage = this.stages['menu'];
        this.current_stage.initialize();
    }

    change_stage(stage_key) {

        this.current_stage.deinitialize();
        this.current_stage = this.stages[stage_key];
        this.current_stage.initialize();
    }

    update(game_time) {
        this.current_stage.update(game_time);
    }
}

class State {

    constructor(){

        this.game_over_text = undefined;
        this.background_image = undefined;
        //this.obstacle_container = new PIXI.ParticleContainer();
    }

    initialize() {

        this.game_objects = [];
        this.obstacle_objects = [];
        this.background_objects = [];

        if (this.background_image !== undefined) {
            stage.addChild(this.background_image);
        }

        //stage.addChild(this.obstacle_container);
    }

    deinitialize() {

        console.log(`stage before ${stage.children.length}`);

        // Game objects
        for (let i = 0; i < this.game_objects.length; i++) {
            stage.removeChild(this.game_objects[i].get_sprite());
        }
        this.game_objects = [];

        // Obstacles
        for (let i = 0; i < this.obstacle_objects.length; i++) {
            stage.removeChild(this.obstacle_objects[i].get_sprite());
        }
        this.obstacle_objects = [];

        // Background objects
        for (let i = 0; i < this.background_objects.length; i++) {
            stage.removeChild(this.background_objects[i]);
        }
        this.background_objects = [];

        if (this.background_image !== undefined) {
            stage.removeChild(this.background_image);
        }

        if (this.game_over_text !== undefined) {
            stage.removeChild(this.game_over_text);
        }
        this.game_over_text = undefined;

        console.log(`stage after ${stage.children.length}`);
    }

    update(game_time) {

    }

    add_game_object(obj) {
        this.game_objects.push(obj);
        stage.addChild(obj.get_sprite());
    }

    add_obstacle_object(obj) {
        this.obstacle_objects.push(obj);
        stage.addChild(obj.get_sprite());
    }

    add_background_object(obj) {
        this.background_objects.push(obj);
        stage.addChild(obj);
    }
}

class LevelState extends State {

    constructor() {
        super();

        this.debug_text = '';
        if (debug_view_on) {
            this.debug_text = new PIXI.Text("Debug text1", {font:"20px Arial", fill:"white"});
            stage.addChild(this.debug_text);
        }

        this.background_image = new PIXI.Sprite(FOREST_ENLARGED);
        this.current_level = 1;
        this.TOT_LEVELS = 8;
    }

    initialize() {
        super.initialize();

        this.player = undefined;
        this.light_manager = undefined;

        this._spawn_initial_setup();

        this.light_manager = new LightManager();
        this.light_manager.assign_light_sources(this.game_objects);
        this.light_manager.generate_darkness();
        this.light_manager.update();

        this.player.assign_light_sources(this.light_manager.get_light_sources(this.game_objects));
        this.game_over = false;
        this.keyboard = new Keyboard();

        //console.log(`stage ${stage.children.length} obst ${this.obstacle_objects.length} objs ${this.game_objects.length} overlay ${this.light_manager.dark_list.length}`);
    }

    deinitialize() {
        super.deinitialize();

        let dark_list = this.light_manager.dark_list;
        for (let i = 0; i < dark_list.length; i++) {
            stage.removeChild(dark_list[i].get_sprite());
        }
        this.light_manager.dark_list = [];
        this.light_manager.dark_matrix = [];

        this.player = undefined;
    }

    _spawn_initial_setup() {

        this.player = new Player(12, 200);
        this.add_game_object(this.player);

        let obstacle_lists = get_level_obstacles(this.current_level);
        let obstacles = obstacle_lists[0];
        let objects = obstacle_lists[1];

        for (let i = 0; i < obstacles.length; i++) {
            this.add_obstacle_object(obstacles[i]);
        }

        for (let i = 0; i < objects.length; i++) {
            this.add_game_object(objects[i]);
        }
    }

    update(game_time) {
        super.update(game_time);

        if (debug_view_on) {
            this._update_debug();
        }

        for (let i = 0; i < this.game_objects.length; i++) {

            let obj = this.game_objects[i];
            obj.update(game_time, this.obstacle_objects, this.game_objects);
        }

        this._remove_the_dead();
        this.light_manager.update(this.game_objects);

        if (this.player.is_dead && !this.game_over) {
            this._game_over_logic();
        }

        if (this.game_over) {
            if (this.keyboard.isKeyPressed('space')) {
                this.deinitialize();
                this.initialize();
            }
        }

        if (this.player.level_win) {
            this.current_level++;
            if (this.current_level <= this.TOT_LEVELS) {
                this.deinitialize();
                this.initialize();
            }
            else {
                this.current_level = 1;
                stage_manager.change_stage('win');
            }
        }
    }

    _game_over_logic() {
        this.game_over_text = new PIXI.Text("Game over - Space to restart", {font:"30px Courier", fill:"white"});
        this.game_over_text.position.x = 40;
        this.game_over_text.position.y = 200;
        stage.addChild(this.game_over_text);

        this.game_over = true;
    }

    _update_debug() {
        this.debug_text.text =
            `x:${Number((this.player.x).toFixed(1))}
y:${Number((this.player.y).toFixed(1))}
ysp:${Number((this.player.yspeed).toFixed(1))}
grounded:${this.player.grounded}
jumping:${this.player.is_jumping}`;
    }

    _remove_the_dead() {
        let marked_by_death_array = [];

        for (let i = 0; i < this.game_objects.length; i++) {
            if (this.game_objects[i].is_dead) {
                marked_by_death_array.push(this.game_objects[i]);
            }
        }

        for (let i = 0; i < marked_by_death_array.length; i++) {
            stage.removeChild(this.game_objects[i].get_sprite());
            this.game_objects.splice(i, 1);
        }
    }
}

class MenuState extends State {

    constructor() {
        super();
        //this.keyboard = new Keyboard();
    }

    initialize() {
        super.initialize();

        let title_text = new PIXI.Text("The Hunted", {font:"50px Courier", fill:"white"});
        //title_text.position.x = 50 - title_text.textWidth / 2;

        title_text.anchor.x = 0.5;
        title_text.position.x = 400;
        title_text.position.y = 50;
        this.add_background_object(title_text);

        let text = new PIXI.Text("Loading assets...", {font:"40px Courier", fill:"white"});
        //text.position.x = 400  - text.textWidth / 2;
        text.anchor.x = 0.5;
        text.position.x = 400;
        text.position.y = 270;
        this.add_background_object(text);

        let helptext = new PIXI.Text("Controls: Arrows", {font:"40px Courier", fill:"white"});
        //helptext.position.x = 400 - helptext.textWidth / 2;
        helptext.anchor.x = 0.5;
        helptext.position.x = 400;
        helptext.position.y = 170;
        this.add_background_object(helptext);

        this.start_time = 0;
        this.delay = 3;
    }

    update(game_time) {
        super.update(game_time);

        if (this.start_time === 0) {
            this.start_time = game_time + this.delay;
        }
        else if (game_time >= this.start_time) {
            FADE_SONG.play();
            stage_manager.change_stage('level');
        }

        //if (this.keyboard.isKeyPressed('space')) {
        //}
    }
}

class WinState extends State {

    constructor() {
        super();
        this.keyboard = new Keyboard();
    }

    initialize() {
        super.initialize();

        let text = new PIXI.Text("You Win!!\nThanks for playing :)\n\nMade by LinkPact Games for Ludum Dare 33\n\nwww.LinkPact.com",
            {font:"30px Courier", fill:"white"});
        text.position.y = 100;
        this.add_background_object(text);
    }

    update(game_time) {
        super.update(game_time);
        if (this.keyboard.isKeyPressed('space')) {
            stage_manager.change_stage('level');
        }
    }
}










