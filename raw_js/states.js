'use strict';

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var StageManagerInstance_temp = (function () {
    function StageManagerInstance_temp() {
        _classCallCheck(this, StageManagerInstance_temp);

        this.stages = {};
        this.stages['menu'] = new MenuState();
        this.stages['level'] = new LevelState();
        this.stages['win'] = new WinState();

        this.current_stage = this.stages['menu'];
        this.current_stage.initialize();
    }

    _createClass(StageManagerInstance_temp, [{
        key: 'change_stage',
        value: function change_stage(stage_key) {

            this.current_stage.deinitialize();
            this.current_stage = this.stages[stage_key];
            this.current_stage.initialize();
        }
    }, {
        key: 'update',
        value: function update(game_time) {
            this.current_stage.update(game_time);
        }
    }]);

    return StageManagerInstance_temp;
})();

var State = (function () {
    function State() {
        _classCallCheck(this, State);

        this.game_over_text = undefined;
        this.background_image = undefined;
        //this.obstacle_container = new PIXI.ParticleContainer();
    }

    _createClass(State, [{
        key: 'initialize',
        value: function initialize() {

            this.game_objects = [];
            this.obstacle_objects = [];
            this.background_objects = [];

            if (this.background_image !== undefined) {
                stage.addChild(this.background_image);
            }

            //stage.addChild(this.obstacle_container);
        }
    }, {
        key: 'deinitialize',
        value: function deinitialize() {

            console.log('stage before ' + stage.children.length);

            // Game objects
            for (var i = 0; i < this.game_objects.length; i++) {
                stage.removeChild(this.game_objects[i].get_sprite());
            }
            this.game_objects = [];

            // Obstacles
            for (var i = 0; i < this.obstacle_objects.length; i++) {
                stage.removeChild(this.obstacle_objects[i].get_sprite());
            }
            this.obstacle_objects = [];

            // Background objects
            for (var i = 0; i < this.background_objects.length; i++) {
                stage.removeChild(this.background_objects[i]);
            }
            this.background_objects = [];

            if (this.background_image !== undefined) {
                stage.removeChild(this.background_image);
            }

            if (this.game_over_text !== undefined) {
                stage.removeChild(this.game_over_text);
            }
            this.game_over_text = undefined;

            console.log('stage after ' + stage.children.length);
        }
    }, {
        key: 'update',
        value: function update(game_time) {}
    }, {
        key: 'add_game_object',
        value: function add_game_object(obj) {
            this.game_objects.push(obj);
            stage.addChild(obj.get_sprite());
        }
    }, {
        key: 'add_obstacle_object',
        value: function add_obstacle_object(obj) {
            this.obstacle_objects.push(obj);
            stage.addChild(obj.get_sprite());
        }
    }, {
        key: 'add_background_object',
        value: function add_background_object(obj) {
            this.background_objects.push(obj);
            stage.addChild(obj);
        }
    }]);

    return State;
})();

var LevelState = (function (_State) {
    _inherits(LevelState, _State);

    function LevelState() {
        _classCallCheck(this, LevelState);

        _get(Object.getPrototypeOf(LevelState.prototype), 'constructor', this).call(this);

        this.debug_text = '';
        if (debug_view_on) {
            this.debug_text = new PIXI.Text("Debug text1", { font: "20px Arial", fill: "white" });
            stage.addChild(this.debug_text);
        }

        this.background_image = new PIXI.Sprite(FOREST_ENLARGED);
        this.current_level = 1;
        this.TOT_LEVELS = 8;
    }

    _createClass(LevelState, [{
        key: 'initialize',
        value: function initialize() {
            _get(Object.getPrototypeOf(LevelState.prototype), 'initialize', this).call(this);

            this.player = undefined;
            this.light_manager = undefined;

            this._spawn_initial_setup();

            this.light_manager = new LightManager();
            this.light_manager.assign_light_sources(this.game_objects);
            this.light_manager.generate_darkness();
            this.light_manager.update();

            this.player.assign_light_sources(this.light_manager.get_light_sources(this.game_objects));
            this.game_over = false;
            this.keyboard = new Keyboard();

            //console.log(`stage ${stage.children.length} obst ${this.obstacle_objects.length} objs ${this.game_objects.length} overlay ${this.light_manager.dark_list.length}`);
        }
    }, {
        key: 'deinitialize',
        value: function deinitialize() {
            _get(Object.getPrototypeOf(LevelState.prototype), 'deinitialize', this).call(this);

            var dark_list = this.light_manager.dark_list;
            for (var i = 0; i < dark_list.length; i++) {
                stage.removeChild(dark_list[i].get_sprite());
            }
            this.light_manager.dark_list = [];
            this.light_manager.dark_matrix = [];

            this.player = undefined;
        }
    }, {
        key: '_spawn_initial_setup',
        value: function _spawn_initial_setup() {

            this.player = new Player(12, 200);
            this.add_game_object(this.player);

            var obstacle_lists = get_level_obstacles(this.current_level);
            var obstacles = obstacle_lists[0];
            var objects = obstacle_lists[1];

            for (var i = 0; i < obstacles.length; i++) {
                this.add_obstacle_object(obstacles[i]);
            }

            for (var i = 0; i < objects.length; i++) {
                this.add_game_object(objects[i]);
            }
        }
    }, {
        key: 'update',
        value: function update(game_time) {
            _get(Object.getPrototypeOf(LevelState.prototype), 'update', this).call(this, game_time);

            if (debug_view_on) {
                this._update_debug();
            }

            for (var i = 0; i < this.game_objects.length; i++) {

                var obj = this.game_objects[i];
                obj.update(game_time, this.obstacle_objects, this.game_objects);
            }

            this._remove_the_dead();
            this.light_manager.update(this.game_objects);

            if (this.player.is_dead && !this.game_over) {
                this._game_over_logic();
            }

            if (this.game_over) {
                if (this.keyboard.isKeyPressed('space')) {
                    this.deinitialize();
                    this.initialize();
                }
            }

            if (this.player.level_win) {
                this.current_level++;
                if (this.current_level <= this.TOT_LEVELS) {
                    this.deinitialize();
                    this.initialize();
                } else {
                    this.current_level = 1;
                    stage_manager.change_stage('win');
                }
            }
        }
    }, {
        key: '_game_over_logic',
        value: function _game_over_logic() {
            this.game_over_text = new PIXI.Text("Game over - Space to restart", { font: "30px Courier", fill: "white" });
            this.game_over_text.position.x = 40;
            this.game_over_text.position.y = 200;
            stage.addChild(this.game_over_text);

            this.game_over = true;
        }
    }, {
        key: '_update_debug',
        value: function _update_debug() {
            this.debug_text.text = 'x:' + Number(this.player.x.toFixed(1)) + '\ny:' + Number(this.player.y.toFixed(1)) + '\nysp:' + Number(this.player.yspeed.toFixed(1)) + '\ngrounded:' + this.player.grounded + '\njumping:' + this.player.is_jumping;
        }
    }, {
        key: '_remove_the_dead',
        value: function _remove_the_dead() {
            var marked_by_death_array = [];

            for (var i = 0; i < this.game_objects.length; i++) {
                if (this.game_objects[i].is_dead) {
                    marked_by_death_array.push(this.game_objects[i]);
                }
            }

            for (var i = 0; i < marked_by_death_array.length; i++) {
                stage.removeChild(this.game_objects[i].get_sprite());
                this.game_objects.splice(i, 1);
            }
        }
    }]);

    return LevelState;
})(State);

var MenuState = (function (_State2) {
    _inherits(MenuState, _State2);

    function MenuState() {
        _classCallCheck(this, MenuState);

        _get(Object.getPrototypeOf(MenuState.prototype), 'constructor', this).call(this);
        //this.keyboard = new Keyboard();
    }

    _createClass(MenuState, [{
        key: 'initialize',
        value: function initialize() {
            _get(Object.getPrototypeOf(MenuState.prototype), 'initialize', this).call(this);

            var title_text = new PIXI.Text("The Hunted", { font: "50px Courier", fill: "white" });
            //title_text.position.x = 50 - title_text.textWidth / 2;

            title_text.anchor.x = 0.5;
            title_text.position.x = 400;
            title_text.position.y = 50;
            this.add_background_object(title_text);

            var text = new PIXI.Text("Loading assets...", { font: "40px Courier", fill: "white" });
            //text.position.x = 400  - text.textWidth / 2;
            text.anchor.x = 0.5;
            text.position.x = 400;
            text.position.y = 270;
            this.add_background_object(text);

            var helptext = new PIXI.Text("Controls: Arrows", { font: "40px Courier", fill: "white" });
            //helptext.position.x = 400 - helptext.textWidth / 2;
            helptext.anchor.x = 0.5;
            helptext.position.x = 400;
            helptext.position.y = 170;
            this.add_background_object(helptext);

            this.start_time = 0;
            this.delay = 3;
        }
    }, {
        key: 'update',
        value: function update(game_time) {
            _get(Object.getPrototypeOf(MenuState.prototype), 'update', this).call(this, game_time);

            if (this.start_time === 0) {
                this.start_time = game_time + this.delay;
            } else if (game_time >= this.start_time) {
                FADE_SONG.play();
                stage_manager.change_stage('level');
            }

            //if (this.keyboard.isKeyPressed('space')) {
            //}
        }
    }]);

    return MenuState;
})(State);

var WinState = (function (_State3) {
    _inherits(WinState, _State3);

    function WinState() {
        _classCallCheck(this, WinState);

        _get(Object.getPrototypeOf(WinState.prototype), 'constructor', this).call(this);
        this.keyboard = new Keyboard();
    }

    _createClass(WinState, [{
        key: 'initialize',
        value: function initialize() {
            _get(Object.getPrototypeOf(WinState.prototype), 'initialize', this).call(this);

            var text = new PIXI.Text("You Win!!\nThanks for playing :)\n\nMade by LinkPact Games for Ludum Dare 33\n\nwww.LinkPact.com", { font: "30px Courier", fill: "white" });
            text.position.y = 100;
            this.add_background_object(text);
        }
    }, {
        key: 'update',
        value: function update(game_time) {
            _get(Object.getPrototypeOf(WinState.prototype), 'update', this).call(this, game_time);
            if (this.keyboard.isKeyPressed('space')) {
                stage_manager.change_stage('level');
            }
        }
    }]);

    return WinState;
})(State);

//# sourceMappingURL=states.js.map