class Animation {

    constructor(texture_sheet, rect, nbr_frames=1, loop_time=0.1) {

        this.frames = [];
        for (let i = 0; i < nbr_frames; i++) {
            let source_rect = new PIXI.Rectangle(rect.x + i * rect.width, rect.y, rect.width, rect.height);
            this.frames.push(new PIXI.Texture(texture_sheet.baseTexture, source_rect))
        }

        this.current_frame = 0;
        this.delta_anim_time = loop_time / this.frames.length;
        this.next_anim_time = 0;

        this.new_frame_ready = false;
    }

    update(game_time) {

        if (this.next_anim_time === 0) {
            this.next_anim_time = game_time + this.delta_anim_time;
        }
        else if (game_time >= this.next_anim_time) {
            this.current_frame++;
            if (this.current_frame >= this.frames.length) {
                this.current_frame = 0;
            }

            this.next_anim_time = game_time + this.delta_anim_time;
            this.new_frame_ready = true;
        }
    }

    has_new_frame() {
        return this.new_frame_ready;
    }

    get_current_frame() {
        this.new_frame_ready = false;
        return this.frames[this.current_frame];
    }
}