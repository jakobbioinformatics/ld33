"use strict";

function correctingColCheck(obj1, obj2) {

    var vectorX = obj1.x + obj1.width / 2 - (obj2.x + obj2.width / 2);
    var vectorY = obj1.y + obj1.height / 2 - (obj2.y + obj2.height / 2);
    var halfWidths = obj1.width / 2 + obj2.width / 2;
    var halfHeights = obj1.height / 2 + obj2.height / 2;
    var colDir = Direction.NONE;

    if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {

        var offsetX = halfWidths - Math.abs(vectorX);
        var offsetY = halfHeights - Math.abs(vectorY);

        if (offsetX >= offsetY) {
            if (vectorY > 0) {
                colDir = Direction.UP;
                obj1.y += offsetY;
            } else {
                colDir = Direction.DOWN;
                obj1.y -= offsetY;
            }
        } else {
            if (vectorX > 0) {
                colDir = Direction.LEFT;
                obj1.x += offsetX;
            } else {
                colDir = Direction.RIGHT;
                obj1.x -= offsetX;
            }
        }
    }
    return colDir;
}

function intersectObjects(obj1, obj2) {
    return !(obj2.x > obj1.x + obj1.width || obj2.x + obj2.width < obj1.x || obj2.y > obj1.y + obj1.height || obj2.y + obj2.height < obj1.y);
}

//# sourceMappingURL=collision_handling.js.map